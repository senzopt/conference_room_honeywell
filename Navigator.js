import Login from './src/Components/Login';
import SettingsForm2 from './src/Components/SettingsForm2';
import slider from './src/Components/slider';
import Dashboard from './src/Components/Dashboard';
import UsernameAdmin from './src/Components/UsernameAdmin';
import { createStackNavigator } from "react-navigation";

const AppNavigator = createStackNavigator(
  {
    Login: {
      screen: Login,
      navigationOptions: {
        header: null
      }
    },
    slider: {
       screen: slider,
       navigationOptions: {
           header: null
       }
    },
    SettingsForm2: {
      screen: SettingsForm2,
      navigationOptions: {
        header: null
      }
    },
    Dashboard: {
      screen: Dashboard,
      navigationOptions: {
        header: null
      }
    },
    UsernameAdmin: {
      screen: UsernameAdmin,
      navigationOptions: {
        header: null
      }
    }
  },

  {
    initialRouteName: "Login"
  }
);

export default AppNavigator;