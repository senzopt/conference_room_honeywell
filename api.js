import RNFetchBlob from "react-native-fetch-blob";
import { AsyncStorage } from "react-native";

export default {
    gateway: {
        devices: async gatewayIp => {
            const token = await AsyncStorage.getItem("token");
            return RNFetchBlob.config({
                trusty: true
            }).fetch(
                "POST",
                `https://${gatewayIp}/api/device`,
                {
                    Authorization: `Bearer ${token}`,
                    "Content-Type": "application/json"
                },
                JSON.stringify({
                    query: [{ deviceId: "*", parameters: ["*"] }]
                })
            );
        },
        auth: {
            login: (gatewayIp, credentials) =>
                RNFetchBlob.config({
                    trusty: true
                }).fetch(
                    "POST",
                    `https://${gatewayIp}/api/login`,
                    { "Content-Type": "application/json" },
                    JSON.stringify({ credentials })
                ),
                
            logout: async (gatewayIp, username) => {
                const token = await AsyncStorage.getItem("token");
                console.log(token);

                return RNFetchBlob.config({
                    trusty: true
                }).fetch(
                    "POST",
                    `https://${gatewayIp}/api/logout`,
                    {
                        Authorization: `Bearer ${token}`,
                        "Content-Type": "application/json"
                    },
                    JSON.stringify({ credentials: { username } })
                );
            }
        }
    }
 };