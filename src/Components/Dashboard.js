import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
} from "react-native";

class DashBoard extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.headerContainer}>
          <TouchableOpacity
            style={{
              marginRight: "36%",
              marginLeft: 15,
              width: 60,
              height: 40
            }}
          >
            <Text style={styles.buttonText}>Back</Text>
          </TouchableOpacity>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              width: 150
            }}
          >
            <Text style={styles.headerText}>ROOM</Text>
          </View>
        </View>
        <View style={styles.middleContainer}>
          <View style={styles.modesContainer}>
            <TouchableOpacity style={styles.modesButton}>
              <Text style={styles.modesButtonText}>Presentation</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.modesButton}>
              <Text style={styles.modesButtonText}>Bright</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.modesButton}>
              <Text style={styles.modesButtonText}>Auto</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.modesButton}>
              <Text style={styles.modesButtonText}>Manual</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.createButtonContainer}>
            <Text
              style={{
                fontWeight: "bold",
                fontSize: 20,
                color: "white",
                width: 200,
                textAlign: "left",
                marginLeft: 10
              }}
            >
              TEMPERATURE:
            </Text>
            <Text
              style={{
                fontWeight: "bold",
                fontSize: 20,
                color: "white",
                width: 200,
                textAlign: "left",
                marginLeft: 10
              }}
            >
              HUMIDITY:
            </Text>
            <Text
              style={{
                fontWeight: "bold",
                fontSize: 20,
                color: "white",
                width: 200,
                textAlign: "left",
                marginLeft: 10
              }}
            >
              LUX VALUE:
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: "center"
  },
  headerContainer: {
    flexDirection: "row",
    height: 70,
    width: "100%",
    backgroundColor: "#d71e25",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  middleContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    width: "100%",
    flex: 5.2
  },
  modesContainer: {
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "flex-start",
    backgroundColor: "white",
    flex: 1.5,
    height: "100%",
    marginLeft: "5%"
  },
  list: {
    paddingVertical: 15,
    margin: 5,
    flexDirection: "row",
    width: "90%",
    backgroundColor: "lightgrey",
    justifyContent: "center",
    alignItems: "center"
  },
  line: {
    height: 0.5,
    width: "100%",
    backgroundColor: "rgba(255,255,255,0.5)"
  },
  deviceButton: {
    width: "80%",
    paddingHorizontal: "10%",
    height: 45,
    backgroundColor: "#d71e25",
    borderRadius: 20
  },
  modesButton: {
    height: 50,
    width: 250,
    borderRadius: 20,
    backgroundColor: "#d71e25",
    justifyContent: "center"
  },

  modesButtonText: {
    fontSize: 18,
    color: "white",
    alignSelf: "center"
  },

  createButtonContainer: {
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "flex-start",
    backgroundColor: 'rgb(215,30,37)',
    flex: 1.5,
    height: "70%",
    marginRight: "5%"
  },
  lowerContainer: {
    flexDirection: "row",
    backgroundColor: "lightgrey",
    justifyContent: "space-around",
    alignItems: "center",
    width: "100%",
    flex: 1
  },

  headerText: {
    fontSize: 20,
    color: "white",
    fontWeight: "bold"
  },
  buttonText: {
    fontSize: 17,
    alignSelf: "center",
    marginTop: 4,
    color: "white"
  },
  headerImage: {
    width: 30,
    height: 30,
    marginRight: 5
  }
});
export default DashBoard;
