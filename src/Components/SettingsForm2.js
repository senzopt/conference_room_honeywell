import React from "react";
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
  StyleSheet,
  AsyncStorage,
  Alert,
  Keyboard,
  TextInput
} from "react-native";
import api from "../../api";

class SettingsForm2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      deviceType: "DALIMASTER",
      gatewayIp: "",
      selectedDaliMaster: "",
      dali: [],
      wirelessP: [],
      thl: []
    };
  }
  AlertFailure = () => {
    Alert.alert("Invalid Token", "Please Login again.", [
      { text: "OK", onPress: () => this.logout() },
      { cancelable: false }
    ]);
  };

  dataForFlatlist = () => {
    const { deviceType } = this.state;
    switch (deviceType) {
      case "DALIMASTER":
        return this.state.dali;
      case "PIR":
        return this.state.wirelessP;
      case "THL":
        return this.state.thl;
      default:
        break;
    }
  };


  logout = async () => {
  const gatewayIp = await AsyncStorage.getItem("gatewayIp");
  const username = await AsyncStorage.getItem("username");
  api.gateway.auth
    .logout(gatewayIp, username)
    .then(() => {
      AsyncStorage.clear();
      this.props.navigation.navigate("Login");
    })
    .catch(e => console.log(e));
};


  daliButtonClick = () => {
    this.setState(state => ((state.deviceType = "DALIMASTER"), state));
    this.refreshData();
  };

  pirButtonClick = () => {
    this.setState(state => ((state.deviceType = "PIR"), state));
    this.refreshData();
  };

  thlButtonClick = () => {
    this.setState(state => ((state.deviceType = "THL"), state));
    this.refreshData();
  };

  setGatewayIp = async () => {
    console.log("Set method called");
     await AsyncStorage.getItem("gatewayIp")
      .then(gatewayIp => {
        console.log("Comission IP, ", gatewayIp);
        api.gateway
          .devices(gatewayIp)
          .then(res => {
            console.log("then called after api");
            const { returnCode } = res.json();
            if (returnCode === -4) {
              this.alertfailure(this.props.navigation);
            } else {
              const { result } =  res.json();
              console.log("result", result);
              if (result) {
                const dali = result
                  .filter(each => each.deviceType === "DALI_MASTER")
                  .map(each => ({
                    label: each.deviceType,
                    value: each.deviceAddress
                  }));
                const wirelessP = result
                  .filter(each => each.deviceType === "OCCUPANCY_SENSOR")
                  .map(each => ({
                    label: each.deviceType,
                    value: each.deviceAddress
                  }));

                const thl = result
                  .filter(each => each.deviceType === "THL_SENSOR")
                  .map(each => ({
                    label: each.deviceType,
                    value: each.deviceAddress
                  }));
                this.setState({
                  dali,
                  wirelessP,
                  thl
                });
                console.log("this.state.dali", this.state.dali);
                console.log("this.state.wirelessP", this.state.wirelessP);
                console.log("this.state.thl", this.state.thl);
              } else {
                Alert.alert(
                  "Error",
                  `No Devices Detected in Network`,
                  [
                    {
                      text: "OK",
                      onPress: () => this.refreshData
                    }
                  ],
                  { cancelable: false }
                );
              }
            }
          })
          .catch(() => {
            Alert.alert(
              "DALI",
              `No DALI MASTER DETECTED IN NETWORK`,
              [
                {
                  text: "OK",
                  onPress: () => this.refreshData
                }
              ],
              { cancelable: false }
            );
          });
      })
      .catch(err => console.log(err));
  };

  componentDidMount() {
    console.log("componentDIDMount called");
     this.setGatewayIp();
    this.refreshData();
  }

  refreshData = async() => {
    console.log("refreshing")
    const { gatewayIp } = this.state;
    await api.gateway
      .devices(gatewayIp)
      .then(res => {
        const { returnCode } = res.json();
        if (returnCode === -4) {
          this.alertfailure(this.props.navigation);
        } else {
          const { result } = res.json();
          if (result) {
            const dali = result
              .filter(each => each.deviceType === "DALI_MASTER")
              .map(each => ({
                label: each.deviceType,
                value: each.deviceAddress
              }));
           const wirelessP = result
             .filter(each => each.deviceType === "OCCUPANCY_SENSOR")
             .map(each => ({
               label: each.deviceType,
               value: each.deviceAddress
             }));
           const thl = result
             .filter(each => each.deviceType === "THL_SENSOR")
             .map(each => ({
               label: each.deviceType,
               value: each.deviceAddress
             }));
             this.setState({
               dali,
               wirelessP,
               thl
             });
          } else {
            Alert.alert(
              "Error",
              `No Devices Detected in Network`,
              [
                {
                  text: "OK",
                  onPress: () => {}
                }
              ],
              { cancelable: false }
            );
          }
        }
      })
      .catch(err => err);
  };

  FlatListItemSeparator = () => <View style={styles.line} />;

  selectItem = data => {
    data.item.isSelect = !data.item.isSelect;
    data.item.selectedClass = data.item.isSelect
      ? styles.selected
      : styles.list;

    const index = this.state.dali.findIndex(item => data.item.id === item.id);

    this.state.dali[index] = data.item;

    this.setState({
      dali: this.state.dali
    });
  };

  renderItem = data => (
    <TouchableOpacity
      style={[styles.list, data.item.selectedClass]}
      onPress={() => this.selectItem(data)}
    >
      <Text style={styles.lightText}> {data.item.label} </Text>
      <Text style={styles.lightText}>{data.item.value}</Text>
    </TouchableOpacity>
  );

  createButtonClick = () => {
    this.props.navigation.navigate("Dashboard");
  };

  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={styles.mainContainer}>
        <View style={styles.headerContainer}>
          <TouchableOpacity
            style={{
              marginLeft: 15,
              width: 60,
              height: 40
            }}
          >
            <Text style={styles.buttonText}>Back</Text>
          </TouchableOpacity>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              width: 150
            }}
          >
            <Text style={styles.headerText}>SETTINGS</Text>
          </View>
          <TouchableOpacity
            style={{
              // marginLeft: "36%",
              marginRight: 5,
              width: 100,
              height: 40
            }}
          >
            <Text style={styles.buttonText} onPress={this.logout}>
              Logout
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.middleContainer}>
          <View style={styles.upperContainer}>
            <View style={styles.deviceButtonsContainer}>
              <TextInput
                underlineColorAndroid={"rgba(0,0,0,0)"}
                autoCapitalize={"none"}
                placeholder="Enter Room name"
                placeholderTextColor="white"
                returnKeyType={"done"}
                style={styles.textInput}
              />
              <TouchableOpacity
                style={styles.deviceButton}
                onPress={this.daliButtonClick}
              >
                <Text style={styles.buttonText}>DALI MASTERS</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.deviceButton}
                onPress={this.pirButtonClick}
              >
                <Text style={styles.buttonText}>OCCUPANCY_SENSORS</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.deviceButton}
                onPress={this.thlButtonClick}
              >
                <Text style={styles.buttonText}>THL_Sensors</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.flatlistContainer}>
              <FlatList
                data={this.dataForFlatlist()}
                ItemSeparatorComponent={this.FlatListItemSeparator}
                renderItem={item => this.renderItem(item)}
                keyExtractor={item => item.value.toString()}
                extraData={this.state}
              />
            </View>
            <View style={styles.createButtonContainer}>
              <TouchableOpacity
                onPress={this.createButtonClick}
                style={styles.createButton}
              >
                <Text style={styles.createButtonText}>CREATE ROOM</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.createButton}>
                <Text style={styles.createButtonText}>SAVE MODES</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.lowerContainer}>
            <TouchableOpacity style={styles.modesButton}>
              <Text style={styles.modesButtonText}>Presentation</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.modesButton}>
              <Text style={styles.modesButtonText}>Bright</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.modesButton}>
              <Text style={styles.modesButtonText}>Auto</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      </TouchableWithoutFeedback>
    );
  }
}
const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: "center"
  },
  headerContainer: {
    flexDirection: "row",
    height: 70,
    width: "100%",
    backgroundColor: "#d71e25",
    justifyContent: "space-between",
    alignItems: "center"
  },
  middleContainer: {
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    width: "100%",
    flex: 5.2
  },
  textInput: {
    height: 50,
    width: "80%",
    borderRadius: 20,
    backgroundColor: "lightgrey",
    paddingHorizontal: "10%"
  },
  list: {
    // paddingVertical: 5,
    margin: 5,
    flexDirection: "column",
    width: 280,
    backgroundColor: "lightgrey",
    justifyContent: "center",
    alignItems: "center"
  },
  line: {
    height: 0.5,
    width: "100%",
    backgroundColor: "rgba(255,255,255,0.5)"
  },
  upperContainer: {
    flexDirection: "row",
    // backgroundColor: 'green',
    justifyContent: "flex-start",
    alignItems: "center",
    width: "100%",
    flex: 3
  },
  deviceButtonsContainer: {
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center",
    // backgroundColor: 'grey',
    flex: 1.5,
    height: "100%"
  },
  deviceButton: {
    width: "80%",
    paddingHorizontal: "10%",
    height: 45,
    backgroundColor: "#d71e25",
    borderRadius: 20
  },
  createButton: {
    paddingHorizontal: "10%",
    height: 45,
    width: "83%",
    backgroundColor: "#d71e25",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 20
  },
  createButtonText: {
    fontSize: 16,
    color: "white",
    alignSelf: "center"
  },
  modesButton: {
    height: 45,
    width: 200,
    borderRadius: 20,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center"
  },
  modesButtonText: {
    fontSize: 16,
    color: "black",
    alignSelf: "center"
  },

  flatlistContainer: {
    //backgroundColor: 'orange',
    flex: 2.5,
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
    // borderLeftWidth:0.5,
    // borderRightWidth: 0.5,
    // borderColor:"lightgrey"
  },
  createButtonContainer: {
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center",
    //backgroundColor: 'grey',
    flex: 1,
    height: "100%"
  },
  lowerContainer: {
    flexDirection: "row",
    backgroundColor: "lightgrey",
    justifyContent: "space-around",
    alignItems: "center",
    width: "100%",
    flex: 1
  },
  middleSubContainer: {
    width: "100%",
    height: "100%",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    padding: 5
    // backgroundColor: '#F8F8F8'
  },
  submitButton: {
    height: 40,
    width: "20%",
    backgroundColor: "#d71e25",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 20,
    marginTop: 40
  },
  headerText: {
    fontSize: 20,
    color: "white",
    fontWeight: "bold"
  },
  buttonText: {
    fontSize: 14,
    alignSelf: "center",
    marginTop: 4,
    color: "white"
  },
  headerImage: {
    width: 30,
    height: 30,
    marginRight: 5
  }
});
export default SettingsForm2;
