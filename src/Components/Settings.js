import React from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
  KeyboardAvoidingView,
  AsyncStorage,
  Alert,
  Platform
} from "react-native";


class Settings extends React.Component {
  constructor(props) {
    super(props);
  }

  submitButtonClick = () => {
    this.props.navigation.navigate("SettingsForm2");
  };

  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.mainContainer}>
          <View style={styles.headerContainer}>
            <Text style={styles.headerText}>SETTINGS</Text>
          </View>
          <KeyboardAvoidingView
            style={styles.middleContainer}
            keyboardVerticalOffset={-80}
            behavior="position"
            enabled
          >
            <View style={styles.middleSubContainer}>
              <Text style={styles.createRoomText}> Create New Room</Text>
              <Text style={styles.loginText}>Room Name</Text>
              <TextInput
                underlineColorAndroid={"rgba(0,0,0,0)"}
                autoCapitalize={"none"}
                returnKeyType={"next"}
                placeholder="Enter Room name"
                placeholderTextColor="white"
                onSubmitEditing={() => {
                  this.secondTextInput.focus();
                }}
                blurOnSubmit={false}
                style={styles.textInput}
              />
              <Text style={styles.passwordText}>Gateway Address</Text>
              <TextInput
                ref={input => {
                  this.secondTextInput = input;
                }}
                underlineColorAndroid={"rgba(0,0,0,0)"}
                placeholder="Enter Room name"
                placeholderTextColor="white"
                secureTextEntry={true}
                style={styles.passwordTextInput}
              />
              <TouchableOpacity
                onPress={this.submitButtonClick}
                style={styles.submitButton}
              >
                <Text style={styles.buttonText}>SUBMIT</Text>
              </TouchableOpacity>
            </View>
          </KeyboardAvoidingView>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}
const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    // // flexDirection: "column",
    // padding: 24,
    // justifyContent:'flex-end',
    alignItems: "center"
  },
  headerContainer: {
    flexDirection: "row",
    height: 70,
    width: "100%",
    backgroundColor: "#d71e25",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 50,
    //marginTop: 24
  },
  middleContainer: {
    flexDirection: "column",
    justifyContent: "flex-start",
    width: "70%",
    height: "65%",
    padding: 5
  },
  middleSubContainer: {
    width: "100%",
    height: "100%",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    padding: 5
    // backgroundColor: '#F8F8F8'
  },
  submitButton: {
    height: 40,
    width: "20%",
    backgroundColor: "#d71e25",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 20,
    marginTop: 40
  },
  textInput: {
    height: 50,
    width: "50%",
    borderRadius: 20,
    backgroundColor: "lightgrey",
    paddingHorizontal: "5%"
  },
  passwordTextInput: {
    height: 50,
    width: "50%",
    borderRadius: 20,
    backgroundColor: "lightgrey",
    paddingHorizontal: "5%"
  },
  loginText: {
    fontSize: 16,
    fontWeight: "normal",
    // paddingLeft: 45,
    color: "#7C7B7B",
    marginBottom: 5,
    marginRight: "29%",
    textAlign: "left",
    marginTop: 20
  },
  passwordText: {
    fontSize: 16,
    fontWeight: "normal",
    color: "#7C7B7B",
    marginBottom: 5,
    marginTop: 20,
    marginRight: "23%",
    textAlign: "left"
  },
  headerText: {
    fontSize: 20,
    color: "white",
    fontWeight: "bold"
  },
  buttonText: {
    fontSize: 16,
    color: "white"
  },
  createRoomText: {
    fontSize: 20,
    color: "grey",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center"
  },
  headerImage: {
    width: 30,
    height: 30,
    marginRight: 5
  }
});

export default Settings;
