import React from "react";
import { Alert,TextInput, 
  TouchableWithoutFeedback, 
  StyleSheet, 
  Text, 
  View ,
  TouchableOpacity,
  Image, 
  KeyboardAvoidingView, 
  Keyboard,
  AsyncStorage} from "react-native";
import api from "../../api";
import Loader from "../../ActivityIndicator";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gatewayIP: "",
      username: "",
      password: "",
      gatewayIpError: "",
      usernameError: "",
      passwordError: "",
      animating: false
    };
  }
  _isIPValid = () =>
    /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(
      this.state.gatewayIP
    );

  startActivityIndicator() {
    this.setState({ animating: true });
  }

  closeActivityIndicator() {
    this.setState({ animating: false });
  }

  _loginClick = () => {
    this.startActivityIndicator();
    if (this._isIPValid()) {
      const { gatewayIP, username, password } = this.state;
      this.setState({ loading: true });
      const credentials = {
        username,
        password
      };
      console.log("credentials are:" , credentials)
      api.gateway.auth
        .login(gatewayIP, credentials)
        .then(result => {
           this.closeActivityIndicator();
          const stat = result.json();
          console.log(stat);
          if (stat.returnCode === 0) {
             AsyncStorage.setItem("gatewayIp", gatewayIP);
             AsyncStorage.setItem("username", credentials.username);
             AsyncStorage.setItem("token", stat.token);
            console.log("test - passing data");
            console.log("username is", this.state.username);
            this.props.navigation.navigate("UsernameAdmin");
          } else if (stat.returnCode === -5) {
            Alert.alert(
              "Authorization Error",
              `User already logged in!!`,
              [
                {
                  text: "OK",
                  onPress: () => this.setState({ loading: false })
                }
              ],
              { cancelable: false }
            );
          } else if (stat.returnCode === -6) {
            Alert.alert(
              "Account Locked",
              `Please Wait 30 minutes to login back.`,
              [
                {
                  text: "OK",
                  onPress: () => this.setState({ loading: false })
                }
              ],
              { cancelable: false }
            );
          } else {
            Alert.alert(
              "Invalid Credentials",
              `Please input valid credentials!!`,
              [
                {
                  text: "OK",
                  onPress: () => this.setState({ loading: false })
                }
              ],
              { cancelable: false }
            );
          }
        })
        .catch(() => {
           this.closeActivityIndicator();
          Alert.alert(
            "Network Failure",
            `Please Try again`,
            [
              {
                text: "OK",
                onPress: () => this.setState({ loading: false })
              }
            ],
            { cancelable: false }
          );
        });
    } else {
       this.closeActivityIndicator();
      Alert.alert(
        "Invlaid IP Address",
        `Please Enter valid IP address `,
        [
          {
            text: "OK",
            onPress: () => this.setState({ loading: false })
          }
        ],
        { cancelable: false }
      );
    }
  };

  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.mainContainer}>
          <View style={styles.headerContainer}>
            <Text style={styles.headerText}>CONFERENCE ROOMS</Text>
          </View>
          <View style={styles.logoContainer}>
            <Image
              source={require("../../images/honeywellLogo.jpg")}
              style={styles.logoImage}
            ></Image>
          </View>
          <KeyboardAvoidingView
            style={styles.middleContainer}
            keyboardVerticalOffset={-110}
            behavior="position"
            enabled
          >
            <View style={styles.middleSubContainer}>
              <Text style={styles.gatewayIPText}>Gateway IP Address</Text>
              <TextInput
                underlineColorAndroid={"rgba(0,0,0,0)"}
                autoCapitalize={"none"}
                returnKeyType={"next"}
                placeholder="Enter Gateway Address"
                placeholderTextColor="white"
                // value={this.state.gatewayIP}
                onChangeText={gatewayIP => this.setState({ gatewayIP })}
                onSubmitEditing={() => {
                  this.firstTextInput.focus();
                }}
                blurOnSubmit={false}
                style={styles.textInput}
              />
              <Text style={styles.loginText}>Username</Text>
              <TextInput
                ref={input => {
                  this.firstTextInput = input;
                }}
                underlineColorAndroid={"rgba(0,0,0,0)"}
                autoCapitalize={"none"}
                returnKeyType={"next"}
                placeholder="Enter Username"
                placeholderTextColor="white"
                //value={username}
                onChangeText={username => this.setState({ username })}
                //error={this.state.usernameError}
                onSubmitEditing={() => {
                  this.secondTextInput.focus();
                }}
                blurOnSubmit={false}
                style={styles.textInput}
                // onChangeText={
                //   text =>
                //     this.setState(state => ((state.user.username = text), state)) // placeholder={I18n.get('Username')}
                // }
              />
              <Text style={styles.passwordText}>Password</Text>
              <TextInput
                ref={input => {
                  this.secondTextInput = input;
                }}
                underlineColorAndroid={"rgba(0,0,0,0)"}
                placeholder="Enter Password"
                placeholderTextColor="white"
                onChangeText={password => this.setState({ password })}
                //error = {this.state.passwordError}
                secureTextEntry={true}
                style={styles.passwordTextInput}
              />
              <TouchableOpacity
                onPress={this._loginClick}
                style={styles.loginButton}
              >
                <Text style={styles.buttonText}>LOG IN</Text>
              </TouchableOpacity>
            </View>
          </KeyboardAvoidingView>
          {this.state.animating && <Loader animating={this.state.animating} />}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  headerContainer: {
    flexDirection: "row",
    height: 70,
    width: "100%",
    backgroundColor: "#d71e25",
    //backgroundColor: '#ED2939',
    justifyContent: "center",
    alignItems: "center",
    //marginBottom: 30
  },
  headerText: {
    fontSize: 20,
    fontWeight: "bold",
    color: "white"
  },
  buttonText: {
    fontSize: 16,
    color: "white"
  },
  logoContainer: {
    flexDirection: "row-reverse",
    alignItems: "center",
    height: "18%",
    width: "100%",
    //backgroundColor: "grey"
  },
  logoImage: {
    width: 150,
    height: "100%",
    paddingVertical: "-5%",
    marginRight: 30
  },
  middleContainer: {
    flexDirection: "column",
    justifyContent: "flex-start",
    width: "60%",
    height: "75%",
    padding: 5,
    marginTop: -90
  },
  middleSubContainer: {
    width: "100%",
    height: "100%",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    padding: 5
    // backgroundColor: 'orange',
  },
  loginButton: {
    height: 40,
    width: "20%",
    backgroundColor: "#d71e25",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 20,
    marginTop: 40
  },
  textInput: {
    height: 50,
    width: "50%",
    borderRadius: 20,
    backgroundColor: "lightgrey",
    paddingHorizontal: "5%"
  },
  passwordTextInput: {
    height: 50,
    width: "50%",
    borderRadius: 20,
    backgroundColor: "lightgrey",
    paddingHorizontal: "5%"
  },
  gatewayIPText: {
    fontSize: 16,
    fontWeight: "bold",
    color: "#7C7B7B",
    marginBottom: 5,
    marginRight: "15%",
    textAlign: "left",
    marginTop: 20
  },
  loginText: {
    fontSize: 16,
    fontWeight: "bold",
    color: "#7C7B7B",
    marginBottom: 5,
    marginRight: "30%",
    textAlign: "left",
    marginTop: 20
  },
  passwordText: {
    fontSize: 16,
    fontWeight: "bold",
    color: "#7C7B7B",
    marginBottom: 5,
    marginTop: 20,
    marginRight: "30%",
    textAlign: "left"
  },
  createRoomText: {
    fontSize: 20,
    color: "grey",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center"
  },
  headerImage: {
    width: 35,
    height: 35
  }
});


export default Login;