import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image
 
} from "react-native";

class UsernameAdmin extends React.Component {
  constructor(props) {
    super(props);
  }

  userButtonClick = () => {
    this.props.navigation.navigate("Dashboard");
  };
  adminButtonClick = () => {
    this.props.navigation.navigate("SettingsForm2");
  };

  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.headerContainer}>
          <Text style={styles.headerText}>CHOOSE ONE OPTION</Text>
        </View>
        <View style={styles.middleContainer}>
          <View style={styles.logoContainer}>
            <Image
              source={require("../../images/honeywellLogo.jpg")}
              style={styles.logoImage}
            ></Image>
          </View>
          <View style={styles.buttonsContainer}>
            <TouchableOpacity
              style={styles.createButton}
              onPress={this.userButtonClick}
            >
              <Text style={styles.createButtonText}>USER</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.createButton}
              onPress={this.adminButtonClick}
            >
              <Text style={styles.createButtonText}>ADMIN</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: "center"
  },
  buttonsContainer: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    height: "60%",
    width: "100%"
    //backgroundColor: 'grey'
  },
  headerContainer: {
    flexDirection: "row",
    height: 70,
    width: "100%",
    backgroundColor: "#d71e25",
    justifyContent: "center",
    alignItems: "center"
  },
  headerText: {
    fontSize: 20,
    color: "white",
    fontWeight: "bold"
  },
  middleContainer: {
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    width: "100%",
    flex: 5.2
    // backgroundColor: 'pink'
  },
  logoContainer: {
    justifyContent: 'center',
    alignItems: "center",
    height: "18%",
    width: "100%",
    marginTop:20,
    marginBottom: 30

  },
  logoImage: {
    width: 250,
    height: "100%",
  },
  createButton: {
    paddingHorizontal: "10%",
    height: 45,
    width: "26%",
    backgroundColor: "#d71e25",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 20,
    marginBottom: 30
  },
  createButtonText: {
    fontSize: 14,
    color: "white",
    alignSelf: "center"
  }
});

export default UsernameAdmin;